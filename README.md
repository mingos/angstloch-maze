# Angstloch Maze

Angstloch is a German word, literally meaning "fear hole". It was a hole in the floor, leading down to a cellar or a dungeon.

Angstloch is also a set of 2D map generators useful in roguelike games.

Angstloch Maze, as the name suggests, can be used to crete mazes.

## Usage

### Instantiate

```
const Maze = require("angstloch-maze").Maze;

const angstlochMaze = new Maze();
```

You may provide your own implementations of the random number generators used by AngstLoch. The first argument is a function that takes two arguments and returns a random imteger that is either one of the provided values or something in between. The second one takes no arguments and returns a random float greater than or equal to 0, but less than 1.

```
function randomInt(min, max) {
    return Math.floor(Math.random() * 0x100000000) %
        (max - min + 1) + min;
}

const angstlochMaze = new Maze(randomInt, Math.random);
```

The custom implementations might be useful if you want a seeded RNG that will produce the same results from the same seed.

### Generate a maze

```
let maze = angstlochMaze.generate();
```

The `generate()` method will return an Angstloch `Map` object (defined in the base [Angstloch](https://bitbucket.org/mingos/angstloch) module).

The maze generation is configurable via the `options` argument:

```
let maze = angstlochMaze.generate({
    horizontalBias: 0,
    verticalBias: 0,
    width: 80,
    height: 25,
    loopChance: 1,
    fill: 0.5,
    symmetry: Maze.SYMMETRY_WHIRLPOOL,
    symmetryChance: 1,
    minExits: 2,
    maxExits: 3,
    map: null,
    startX: null,
    startY: null
});
```

#### Available options

* `width` - the generated map width in cells. Defaults to 25.
* `height` - the generated map height in cells. Defaults to 25.
* `horizontalBias` - integer indicating how biased the maze is towards horizontal corridors. Defaults to 0.
* `verticalBias` - integer indicating how biased the maze is towards vertical corridors. Defaults to 0.
* `loopChance` - chance that a dead ends will be converted to loops, where 0 is no chance of creating loops, 1 is guaranteed loops and no dead ends. Defaults to 0.
* `fill` - fraction of the map that is supposed to be filled, where 0 is none, 1 is 100%. Defaults to 1.
* `symmetry` - what kind of symmetry to use: none, whirlpool (the corridors twirl around the centre) or snowflake (the corridors stem from the centre). Defaults to `Maze.SYMMETRY_NONE`. It can be set to `Maze.SYMMETRY_WHIRLPOOL` or `Maze.SYMMETRY_SNOWFLAKE`.
* `symmetryChance` - how strongly the symmetry should be forced. Defaults to 1, but has no effect when symmetry is `Map.SYMMETRY_NONE`. Takes a floating point value <0,1>, where 0 is equivalent to no symmetry and 1 is equivalent to guaranteed forced symmetry.
* `straight` - how much preference for straight corridors the generator will have. Defaults to 0. When at 0, the corridors will be randomly winding (or follow other rules). When at 1, the corridors will end up
* `minExits` - minimum number of randomly placed exits. Defaults to 0.
* `maxExits` - maximum number of randomly placed exits. Defaults to 0.
* `map` - a previously generated input `Map` object to work on. If not specified, a brand new map will be created.
* `startX` - X coordinate of the initial point from which the maze carving takes place. Should be an odd number. If unspecified, a coordinate near the map's centre will be used.
* `startY` - Y coordinate of the intial maze carving point. Again, if unspecified, one near the map's centre will be picked.

All options can be omitted.

#### Comments

`width` and `height` work best if they're odd numbers, since the algorithm will then fill the entire map and leave a single wall frame. Even numbers will result in two cell wide frame at the right and/or bottom sides of the map.
 
 `horizontalBias` and `verticalBias` work together. For example, if horizontal is 1 and the other is 0, it doesn't mean that the maze is slightly biased towards horizontal passages, but rather that whenever possible, all passages will be horizontal. In order to create a slight tendency towards horizontal passages, it's necessary to set the vertical bias to a number less than horizontal bias, nut greater than 0. For example, horizontal = 5 and vertical = 4 will result in only a slight bias. In order to create unbiased corridors, make the values equal or leave them at their default.

Failing to provide the bias options will result in random bias ratio.

`loopChance` is basically a switch between a perfect and a braid maze. A loop chance of 0 will create no loops and all dead ends, resulting in a perfect maze (where for any points A and B there's exactly one route between them). Inversely, if loop chance is 1, the result will be a braid maze, or one with no dead ends, but multiple routes between any points A and B. Values between 1 and 0 will result in some dead ends and some loops.

The loop chance, even if it's 1, will still create a dead end in case the maze fill is below 100%.

Failing to proide the loop chance option will result in a random value.

`fill` translates to the percentage of the maze that's supposed to be filled with passages. 0 will result in a single wall cell in a fully wall-filled map. 1, on the other hand, will fill 100% of the available space. Values in between will fill a proportional part, for example, 0.5 will result in 50% of the maze being filled will corridors.

Note that if the fill is below 100%, a single dead end is guaranteed, regardless of the loop chance setting.

The corridor direction affecting options are taken into consideration in a predefined order, so if one fail, the next is applied. The order is the following: `straight`, `symmetry`, bias. If all three fail to be applied, the digging direction is completely random.

### Add exits

If the generator doesn't automatically add random exits, it's possible to add them after the maze has been generated using the `Maze#addExit()` method.

The method has the following signature:

```
addExit(map: Map, direction?: Point): void;
```

So, it can be used like this:

```
let maze = angstlochMaze.generate(options);
angstlochMaze.addExit(maze, Maze.DIRECTION_NORTH);
angstlochMaze.addExit(maze);
```

The `map` argument is any `Map` object, not necessarily generated by the maze generator.

The `direction` argument is optional. If specified, it will create the exit on the desired side of the map. Otherwise, the exit placement will be random.

## Some example outputs

Mostly default settings, no randomly generated exits (exits on the north and south sides added after the map generation), with a small chance of looping the corridors:

```
const options = {
    width: 33,
    height: 33,
    loopChance: 0.25
}
let maze = angstlochMaze.generate(options);
angstlochMaze.addExit(maze, Maze.DIRECTION_NORTH);
angstlochMaze.addExit(maze, Maze.DIRECTION_SOUTH);
```
![maze 1](https://bytebucket.org/mingos/angstloch-maze/raw/fdd04b3a72836820c923cf204e23b2ba257121b7/img/1.png)

---

No loops (perfect maze) with some randomly generated exits and whirlpool symmetry:

```
const options = {
    width: 33,
    height: 33,
    symmetry: Maze.SYMMETRY_WHIRLPOOL,
    minExits: 2,
    maxExits: 4
}
let maze = angstlochMaze.generate(options);
```
![maze 2](https://bytebucket.org/mingos/angstloch-maze/raw/fdd04b3a72836820c923cf204e23b2ba257121b7/img/2.png)

---

Notable preference for straight corridors, with no symmetry or bias:

```
const options = {
    width: 33,
    height: 33,
    straight: 0.75,
    minExits: 2,
    maxExits: 2
}
let maze = angstlochMaze.generate(options);
```
![maze 3](https://bytebucket.org/mingos/angstloch-maze/raw/fdd04b3a72836820c923cf204e23b2ba257121b7/img/3.png)

---

A braid maze (no dead ends) with a notable bias for horizontal corridors:

```
const options = {
    horizontalBias: 3,
    verticalBias: 1,
    width: 33,
    height: 33,
    loopChance: 1,
    minExits: 2,
    maxExits: 4
}
let maze = angstlochMaze.generate(options);
```
![maze 4](https://bytebucket.org/mingos/angstloch-maze/raw/fdd04b3a72836820c923cf204e23b2ba257121b7/img/4.png)

---

A partially filled maze with some loops:

```
const options = {
    width: 33,
    height: 33,
    loopChance: 0.5,
    fill: 0.75,
    minExits: 2,
    maxExits: 4
}
let maze = angstlochMaze.generate(options);
```
![maze 5](https://bytebucket.org/mingos/angstloch-maze/raw/fdd04b3a72836820c923cf204e23b2ba257121b7/img/5.png)

All images have been generated using [Angstloch Image](https://bitbucket.org/mingos/angstloch-image);
