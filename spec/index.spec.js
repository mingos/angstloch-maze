const expect = require("chai").expect;
const stub = require("sinon").stub;

const Maze = require("../lib/index").Maze;
const Point = require("angstloch").Point;
const Cell = require("angstloch").Cell;
const Map = require("angstloch").Map;

describe("Maze", () => {
	let maze;
	let map;

	beforeEach(() => {
		map = new Map(15, 15);
		maze = new Maze();
		maze._map = map;
	});

	it("should determine straight direction", () => {
		// given
		const position = new Point(10, 10);
		const possibleDirections = [Maze.DIRECTIONS[0], Maze.DIRECTIONS[1]]; // NORTH & EAST

		stub(maze._map, "getCellType").callsFake((pos) => {
			return (pos.x === position.x - Maze.DIRECTIONS[0].x / 2 &&
					pos.y === position.y - Maze.DIRECTIONS[0].y / 2) ?
				Cell.TYPE_FLOOR :
				Cell.TYPE_WALL;
		});

		// when
		const result = maze._getStraightDirection(position, possibleDirections);

		// then
		expect(result).to.deep.equal(Maze.DIRECTIONS[0]);
	});

	it("should generate random odd numbers", () => {
		// given
		let results = [];

		// when
		for (let i = 0; i < 100; ++i) {
			results.push(maze._randomOdd(maze._random(1, 100), maze._random(101, 200)));
		}

		// then
		let result = results.map(item => item % 2).reduce((a, b) => a + b);
		expect(result).to.equal(100);
	});

	it("should ensure that two cells are connected", () => {
		// given
		map.setCellType(1, 1, Cell.TYPE_FLOOR);
		map.setCellType(2, 1, Cell.TYPE_FLOOR);

		// when
		let result = maze._isConnected(new Point(1, 1), new Point(2, 0));

		// then
		expect(result).to.be.true;
	});

	it("should ensure that two cells are not connected", () => {
		// given
		map.setCellType(1, 1, Cell.TYPE_FLOOR);

		// when
		let result = maze._isConnected(new Point(1, 1), new Point(2, 0));

		// then
		expect(result).to.be.false;
	});

	it("should dig a section of corridor", () => {
		// given
		map.setCellType(1, 1, Cell.TYPE_FLOOR);

		// when
		maze._dig(new Point(1, 1), new Point(2, 0));

		// then
		expect(map.getCellType(2, 1)).to.equal(Cell.TYPE_FLOOR);
		expect(map.getCellType(3, 1)).to.equal(Cell.TYPE_FLOOR);
	});

	it("should ensure a cell is visited", () => {
		// given
		map.setCellType(1, 1, Cell.TYPE_FLOOR);
		map.setCellType(3, 1, Cell.TYPE_FLOOR);

		// when
		let result = maze._isVisited(new Point(1, 1), new Point(2, 0));

		// then
		expect(result).to.be.true;
	});

	it("should ensure a cell is not visited", () => {
		// given
		map.setCellType(1, 1, Cell.TYPE_FLOOR);

		// when
		let result = maze._isVisited(new Point(1, 1), new Point(2, 0));

		// then
		expect(result).to.be.false;
	});

	it("should ensure a cell is within the map bounds", () => {
		// when
		let result = maze._isInMap(new Point(1, 1), new Point(2, 0));

		// then
		expect(result).to.be.true;
	});

	it("should ensure a cell is not within the map bounds", () => {
		// when
		let result = maze._isInMap(new Point(1, 1), new Point(-2, 0)) ||
			maze._isInMap(new Point(1, 1), new Point(0, -2)) ||
			maze._isInMap(new Point(13, 13), new Point(2, 0)) ||
			maze._isInMap(new Point(13, 13), new Point(0, 2));

		// then
		expect(result).to.be.false;
	});

	it("should mark all unvisited positions as possible", () => {
		// given
		map.setCellType(7, 7, Cell.TYPE_FLOOR);

		// when
		let result = maze._getPossibleDirections(new Point(7, 7));

		// then
		expect(result).to.contain(new Point(2, 0));
		expect(result).to.contain(new Point(-2, 0));
		expect(result).to.contain(new Point(0, 2));
		expect(result).to.contain(new Point(0, -2));
	});

	it("should mark only unvisited positions as possible", () => {
		// given
		map.setCellType(7, 7, Cell.TYPE_FLOOR);
		map.setCellType(9, 7, Cell.TYPE_FLOOR);
		map.setCellType(7, 9, Cell.TYPE_FLOOR);

		// when
		let result = maze._getPossibleDirections(new Point(7, 7));

		// then
		expect(result).not.to.contain(new Point(2, 0));
		expect(result).to.contain(new Point(-2, 0));
		expect(result).not.to.contain(new Point(0, 2));
		expect(result).to.contain(new Point(0, -2));
	});
});
