const gulp = require("gulp");
const mocha = require("gulp-mocha");
const tsc = require("gulp-typescript");
const merge = require("merge2");
const runSequence = require("run-sequence");

gulp.task("test", () => {
	return gulp.src("spec/**/*.spec.js")
		.pipe(mocha());
});

gulp.task("typescript", () => {
	const result = gulp.src("src/**/*.ts")
		.pipe(tsc({
			declaration: true,
			target: "ES5",
			removeComments: true
		}));

	return merge([
		result.dts.pipe(gulp.dest("types/")),
		result.js.pipe(gulp.dest("lib/"))
	]);
});

gulp.task("rebuild", () => runSequence(["typescript", "test"]));

gulp.task("watch", () => {
	gulp.watch(["spec/**/*.spec.js"], ["test"]);
	gulp.watch(["src/**/*.ts"], ["rebuild"]);
});
