import {Map, Point, Cell, Offsets, Utils} from "angstloch";
import {IOptions} from "./i-options";

let defaultRandom = function(min: number, max: number): number {
	if (max == min) {
		return min;
	} else if (min > max) {
		let tmp = min;
		min = max;
		max = tmp;
	}
	return Math.floor(Math.random() * 0x100000000) %
		(max - min + 1) + min;
};
let defaultFrandom = Math.random;
let defaultRandomOdd = function(min: number, max: number): number {
	let result: number;
	do {
		result = this._random(min, max)
	} while (result % 2 === 0);
	return result;
};

export class Maze {
	public static SYMMETRY_NONE: number = 0;
	public static SYMMETRY_WHIRLPOOL: number = 1;
	public static SYMMETRY_SNOWFLAKE: number = 2;

	private static DIRECTIONS: Array<Point> = Offsets.map(offset => offset.translate(offset));

	private _random: (min: number, max: number) => number;
	private _randomOdd: (min: number, max: number) => number;
	private _frandom: () => number;
	private _map: Map;
	private _visited: Array<Point>;
	private _options: IOptions;

	constructor(random?: (min: number, max: number) => number, frandom?: () => number) {
		this._random = random || defaultRandom;
		this._randomOdd = defaultRandomOdd.bind(this);
		this._frandom = frandom || defaultFrandom;
	}

	private _getDefaultOptions(): IOptions {
		return {
			horizontalBias: 0,
			verticalBias: 0,
			width: 25,
			height: 25,
			loopChance: 0,
			fill: 1,
			symmetry: Maze.SYMMETRY_NONE,
			symmetryChance: 1,
			straight: 0,
			minExits: 0,
			maxExits: 0
		};
	}

	public generate(options: IOptions): Map {
		this._options = this._buildOptions(options);
		this._map = this._getMap(this._options);
		this._visited = [];

		let initialPoint: Point = this._getInitialPoint();
		this._map.setCellType(initialPoint, Cell.TYPE_FLOOR);
		this._visited.push(initialPoint);

		this._digMaze(this._calculateMaxFilled(this._map));
		this._addExits();

		return this._map;
	}

	public addExit(map: Map, direction?: Point): void {
		direction = direction || Maze.DIRECTIONS[this._random(0, Maze.DIRECTIONS.length - 1)];

		// normalise the direction offset
		direction = new Point(Math.max(-1, Math.min(1, direction.x)), Math.max(-1, Math.min(1, direction.y)));

		// pick coords
		let x: number;
		let y: number;
		do {
			x = direction.x < 0 ?
				0 :
				(direction.x > 0 ?
					map.width - 1 :
					this._randomOdd(1, map.width - 2));
			y = direction.y < 0 ?
				0 :
				(direction.y > 0 ?
					map.height - 1 :
					this._randomOdd(1, map.height - 2));
		} while (map.getCellType(x, y) != Cell.TYPE_WALL);

		const doorPosition = new Point(x, y);

		// dig tunnel from the door to the nearest corridor
		map.setCellType(doorPosition, Cell.TYPE_DOOR);
		map.setCellType(doorPosition.translate(-direction.x, -direction.y), Cell.TYPE_FLOOR);

		// TODO special treatment of partially filled maps; now possible to add doors disconnected from the corridors!
	}

	private _digMaze(maxFilled: number): void {
		let filled = 1;

		// repeat until we've removed all visited cells from the visited stack or filled enough cells
		while(this._visited.length && filled / maxFilled < this._options.fill) {
			let current: Point = this._visited[this._visited.length - 1];

			let possibleDirections: Array<Point> = this._getPossibleDirections(current);
			if (possibleDirections.length) {
				// select the appropriate direction and dig, pushing the new cell onto the visited stack
				let direction: Point = this._selectDirection(current, possibleDirections);
				this._dig(current, direction);
				this._visited.push(current.translate(direction));
				filled++;
			} else {
				// no possible directions, so we pop the cell from the stack
				this._visited.pop();

				// consider creating a loop, but only when we're currently at a dead end
				if (this._isDeadEnd(current) && this._options.loopChance > this._frandom()) {
					this._createLoop(current);
				}
			}
		}
	}

	private _addExits(): void {
		if (this._options.minExits) {
			for (let exits = this._random(this._options.minExits, this._options.maxExits); exits > 0; --exits) {
				this.addExit(this._map);
			}
		}
	}

	private _buildOptions(options: IOptions): IOptions {
		let result: IOptions = this._getDefaultOptions();
		for (let p in options) {
			if (options.hasOwnProperty(p)) {
				result[p] = options[p];
			}
		}

		return result;
	}

	private _getMap(options: IOptions): Map {
		if (options.map) {
			return options.map;
		} else {
			let map: Map = new Map(options.width, options.height);

			// prefill map with walls
			for (let x = 0; x < map.width; ++x) {
				for (let y = 0; y < map.height; ++y) {
					map.setCellType(x, y, Cell.TYPE_WALL);
				}
			}

			return map;
		}
	}

	private _createLoop(position: Point) {
		let possibleDirections: Array<Point> = this._getPossibleLoopDirections(position);
		if (possibleDirections.length) {
			let direction: Point = this._selectDirection(position, possibleDirections);
			this._dig(position, direction);
		}
	}

	private _getInitialPoint(): Point {
		return new Point(
			typeof this._options.startX === "number" ?
				this._options.startX :
				1 + 2 * Math.floor(this._map.width / 4),
			typeof this._options.startY === "number" ?
				this._options.startY :
				1 + 2 * Math.floor(this._map.height / 4)
		);
	}

	private _calculateMaxFilled(map): number {
		return (map.width - (map.width % 2 == 0 ? 2 : 1)) / 2 *
			(map.height - (map.height % 2 == 0 ? 2 : 1)) / 2;
	}

	private _getPossibleDirections(position: Point): Array<Point> {
		return Maze.DIRECTIONS.filter(direction => {
			return this._isInMap(position, direction) && !this._isVisited(position, direction);
		});
	}

	private _isInMap(position: Point, direction: Point): boolean {
		const newPosition = position.translate(direction);

		return newPosition.x >= 1 && newPosition.x <= this._map.width - 2 &&
			newPosition.y >= 1 && newPosition.y <= this._map.height - 2;
	}

	private _isVisited(position: Point, direction: Point): boolean {
		return this._map.getCellType(position.translate(direction)) === Cell.TYPE_FLOOR;
	}

	private _selectDirection(position: Point, possibleDirections: Array<Point>): Point {
		let straightDirection: Point = null;
		let offsetDirection: Point = null;
		let biasedDirection: Point = null;

		if (this._options.straight > this._frandom()) {
			straightDirection = this._getStraightDirection(position, possibleDirections);
		}

		if (this._options.symmetry != Maze.SYMMETRY_NONE && this._options.symmetryChance > this._frandom()) {
			const centre: Point = new Point(Math.floor(this._map.width / 2), Math.floor(this._map.height / 2));
			const offset = this._options.symmetry === Maze.SYMMETRY_WHIRLPOOL ?
				Utils.getPerpendicularOffset(centre, position) :
				new Point(position.x - centre.x, position.y - centre.y);

			offsetDirection = this._getOffsetDirection(offset, possibleDirections);
		}

		if (this._options.horizontalBias != this._options.verticalBias) {
			biasedDirection = this._getBiasedDirection(possibleDirections);
		}

		return straightDirection || offsetDirection || biasedDirection || this._getRandomDirection(possibleDirections);
	}

	private _dig(position: Point, direction: Point): void {
		this._map.setCellType(position.translate(direction.x / 2, direction.y / 2), Cell.TYPE_FLOOR);
		this._map.setCellType(position.translate(direction), Cell.TYPE_FLOOR);
	}

	private _isDeadEnd(position: Point): boolean {
		return Maze.DIRECTIONS.filter(direction => {
			return this._isInMap(position, direction) && this._isConnected(position, direction);
		}).length === 1;
	}

	private _getPossibleLoopDirections(position: Point): Array<Point> {
		return Maze.DIRECTIONS.filter(direction => {
			return this._isInMap(position, direction) && !this._isConnected(position, direction);
		});
	}

	private _tryHorizontalDirection(possibleDirections: Array<Point>): Point {
		return this._getBiasedDirectionIfPossible(possibleDirections, (direction: Point) => {
			return !direction.y;
		});
	}

	private _tryVerticalDirection(possibleDirections: Array<Point>): Point {
		return this._getBiasedDirectionIfPossible(possibleDirections, (direction: Point) => {
			return !direction.x;
		});
	}

	private _getBiasedDirectionIfPossible(possibleDirections: Array<Point>,
	                                      filterFunction: (i: any) => boolean): Point {
		const pruned: Array<Point> = possibleDirections.filter(filterFunction);
		return pruned.length ?
			pruned[this._random(0, pruned.length - 1)] :
			null;
	}

	private _getBiasedDirection(possibleDirections: Array<Point>): Point {
		const selected: number = this._random(1, this._options.horizontalBias + this._options.verticalBias);

		return selected <= this._options.horizontalBias ?
			this._tryHorizontalDirection(possibleDirections) :
			this._tryVerticalDirection(possibleDirections);
	}

	private _getOffsetDirection(offset: Point, possibleDirections: Array<Point>): Point {
		const selected = this._random(1, Math.abs(offset.x) + Math.abs(offset.y));

		return selected <= Math.abs(offset.x) ?
			this._tryHorizontalDirection(possibleDirections) :
			this._tryVerticalDirection(possibleDirections);
	}

	private _getStraightDirection(position: Point, possibleDirections: Array<Point>): Point {
		let potentialDirections: Array<Point> = possibleDirections.filter(dir => {
			return this._map.getCellType(position.translate(-dir.x / 2, -dir.y / 2)) === Cell.TYPE_FLOOR;
		});

		if (potentialDirections.length) {
			return potentialDirections[this._random(0, potentialDirections.length - 1)];
		}

		return null;
	}

	private _getRandomDirection(possibleDirections: Array<Point>): Point {
		return possibleDirections[this._random(0, possibleDirections.length - 1)];
	}

	private _isConnected(position: Point, direction: Point): boolean {
		return this._map.getCellType(position.translate(direction.x / 2, direction.y / 2)) === Cell.TYPE_FLOOR;
	}
}
