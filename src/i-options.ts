import {Map} from "angstloch";

export interface IOptions {
	map?: Map;
	width?: number;
	height?: number;
	startX?: number;
	startY?: number;
	horizontalBias?: number;
	verticalBias?: number;
	loopChance?: number;
	fill?: number;
	symmetry?: number;
	symmetryChance?: number;
	straight?: number;
	minExits?: number;
	maxExits?: number;
}
