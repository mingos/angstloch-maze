const Maze = require("./index").Maze;
const Cell = require("angstloch").Cell;
const colors = require("colors");
const Image = require("angstloch-image").Image;
const path = require("path");
const Offsets = require("angstloch").Offsets;
const Map = require("angstloch").Map;

let map = new Map(80, 25);
for (let x = 0; x < map.width; ++x) {
	for (let y = 0; y < map.height; ++y) {
		map.setCellType(x, y, Cell.TYPE_WALL);
	}
}

const TILES = {};
TILES[Cell.TYPE_FLOOR] = " ";
TILES[Cell.TYPE_WALL] = "#".grey;
TILES[Cell.TYPE_DOOR] = "+".red;

const angstlochMaze = new Maze();
const maze = angstlochMaze.generate({
	horizontalBias: 0,
	verticalBias: 0,
	width: 80,
	height: 25,
	loopChance: 0.5,
	fill: 1,
	symmetry: Maze.SYMMETRY_NONE,
	symmetryChance: 1,
	straight: 0,
	// minExits: 2,
	// maxExits: 4,
	// map: map
});

angstlochMaze.addExit(maze, Offsets[0]);
angstlochMaze.addExit(maze, Offsets[2]);

const display = [];

maze.forEachCell((x, y, cell) => {
	display[y] = display[y] || [];
	display[y][x] = TILES[cell.type];
});

console.log("options:".green)
console.log(JSON.stringify(angstlochMaze._options, null, "    ") + "\n");

display.forEach(row => {
	console.log(row.join(""));
});

// const imageGenerator = new Image();
// imageGenerator.generate(maze, path.join(__dirname, "../output.png"));
