"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var angstloch_1 = require("angstloch");
var defaultRandom = function (min, max) {
    if (max == min) {
        return min;
    }
    else if (min > max) {
        var tmp = min;
        min = max;
        max = tmp;
    }
    return Math.floor(Math.random() * 0x100000000) %
        (max - min + 1) + min;
};
var defaultFrandom = Math.random;
var defaultRandomOdd = function (min, max) {
    var result;
    do {
        result = this._random(min, max);
    } while (result % 2 === 0);
    return result;
};
var Maze = (function () {
    function Maze(random, frandom) {
        this._random = random || defaultRandom;
        this._randomOdd = defaultRandomOdd.bind(this);
        this._frandom = frandom || defaultFrandom;
    }
    Maze.prototype._getDefaultOptions = function () {
        return {
            horizontalBias: 0,
            verticalBias: 0,
            width: 25,
            height: 25,
            loopChance: 0,
            fill: 1,
            symmetry: Maze.SYMMETRY_NONE,
            symmetryChance: 1,
            straight: 0,
            minExits: 0,
            maxExits: 0
        };
    };
    Maze.prototype.generate = function (options) {
        this._options = this._buildOptions(options);
        this._map = this._getMap(this._options);
        this._visited = [];
        var initialPoint = this._getInitialPoint();
        this._map.setCellType(initialPoint, angstloch_1.Cell.TYPE_FLOOR);
        this._visited.push(initialPoint);
        this._digMaze(this._calculateMaxFilled(this._map));
        this._addExits();
        return this._map;
    };
    Maze.prototype.addExit = function (map, direction) {
        direction = direction || Maze.DIRECTIONS[this._random(0, Maze.DIRECTIONS.length - 1)];
        direction = new angstloch_1.Point(Math.max(-1, Math.min(1, direction.x)), Math.max(-1, Math.min(1, direction.y)));
        var x;
        var y;
        do {
            x = direction.x < 0 ?
                0 :
                (direction.x > 0 ?
                    map.width - 1 :
                    this._randomOdd(1, map.width - 2));
            y = direction.y < 0 ?
                0 :
                (direction.y > 0 ?
                    map.height - 1 :
                    this._randomOdd(1, map.height - 2));
        } while (map.getCellType(x, y) != angstloch_1.Cell.TYPE_WALL);
        var doorPosition = new angstloch_1.Point(x, y);
        map.setCellType(doorPosition, angstloch_1.Cell.TYPE_DOOR);
        map.setCellType(doorPosition.translate(-direction.x, -direction.y), angstloch_1.Cell.TYPE_FLOOR);
    };
    Maze.prototype._digMaze = function (maxFilled) {
        var filled = 1;
        while (this._visited.length && filled / maxFilled < this._options.fill) {
            var current = this._visited[this._visited.length - 1];
            var possibleDirections = this._getPossibleDirections(current);
            if (possibleDirections.length) {
                var direction = this._selectDirection(current, possibleDirections);
                this._dig(current, direction);
                this._visited.push(current.translate(direction));
                filled++;
            }
            else {
                this._visited.pop();
                if (this._isDeadEnd(current) && this._options.loopChance > this._frandom()) {
                    this._createLoop(current);
                }
            }
        }
    };
    Maze.prototype._addExits = function () {
        if (this._options.minExits) {
            for (var exits = this._random(this._options.minExits, this._options.maxExits); exits > 0; --exits) {
                this.addExit(this._map);
            }
        }
    };
    Maze.prototype._buildOptions = function (options) {
        var result = this._getDefaultOptions();
        for (var p in options) {
            if (options.hasOwnProperty(p)) {
                result[p] = options[p];
            }
        }
        return result;
    };
    Maze.prototype._getMap = function (options) {
        if (options.map) {
            return options.map;
        }
        else {
            var map = new angstloch_1.Map(options.width, options.height);
            for (var x = 0; x < map.width; ++x) {
                for (var y = 0; y < map.height; ++y) {
                    map.setCellType(x, y, angstloch_1.Cell.TYPE_WALL);
                }
            }
            return map;
        }
    };
    Maze.prototype._createLoop = function (position) {
        var possibleDirections = this._getPossibleLoopDirections(position);
        if (possibleDirections.length) {
            var direction = this._selectDirection(position, possibleDirections);
            this._dig(position, direction);
        }
    };
    Maze.prototype._getInitialPoint = function () {
        return new angstloch_1.Point(typeof this._options.startX === "number" ?
            this._options.startX :
            1 + 2 * Math.floor(this._map.width / 4), typeof this._options.startY === "number" ?
            this._options.startY :
            1 + 2 * Math.floor(this._map.height / 4));
    };
    Maze.prototype._calculateMaxFilled = function (map) {
        return (map.width - (map.width % 2 == 0 ? 2 : 1)) / 2 *
            (map.height - (map.height % 2 == 0 ? 2 : 1)) / 2;
    };
    Maze.prototype._getPossibleDirections = function (position) {
        var _this = this;
        return Maze.DIRECTIONS.filter(function (direction) {
            return _this._isInMap(position, direction) && !_this._isVisited(position, direction);
        });
    };
    Maze.prototype._isInMap = function (position, direction) {
        var newPosition = position.translate(direction);
        return newPosition.x >= 1 && newPosition.x <= this._map.width - 2 &&
            newPosition.y >= 1 && newPosition.y <= this._map.height - 2;
    };
    Maze.prototype._isVisited = function (position, direction) {
        return this._map.getCellType(position.translate(direction)) === angstloch_1.Cell.TYPE_FLOOR;
    };
    Maze.prototype._selectDirection = function (position, possibleDirections) {
        var straightDirection = null;
        var offsetDirection = null;
        var biasedDirection = null;
        if (this._options.straight > this._frandom()) {
            straightDirection = this._getStraightDirection(position, possibleDirections);
        }
        if (this._options.symmetry != Maze.SYMMETRY_NONE && this._options.symmetryChance > this._frandom()) {
            var centre = new angstloch_1.Point(Math.floor(this._map.width / 2), Math.floor(this._map.height / 2));
            var offset = this._options.symmetry === Maze.SYMMETRY_WHIRLPOOL ?
                angstloch_1.Utils.getPerpendicularOffset(centre, position) :
                new angstloch_1.Point(position.x - centre.x, position.y - centre.y);
            offsetDirection = this._getOffsetDirection(offset, possibleDirections);
        }
        if (this._options.horizontalBias != this._options.verticalBias) {
            biasedDirection = this._getBiasedDirection(possibleDirections);
        }
        return straightDirection || offsetDirection || biasedDirection || this._getRandomDirection(possibleDirections);
    };
    Maze.prototype._dig = function (position, direction) {
        this._map.setCellType(position.translate(direction.x / 2, direction.y / 2), angstloch_1.Cell.TYPE_FLOOR);
        this._map.setCellType(position.translate(direction), angstloch_1.Cell.TYPE_FLOOR);
    };
    Maze.prototype._isDeadEnd = function (position) {
        var _this = this;
        return Maze.DIRECTIONS.filter(function (direction) {
            return _this._isInMap(position, direction) && _this._isConnected(position, direction);
        }).length === 1;
    };
    Maze.prototype._getPossibleLoopDirections = function (position) {
        var _this = this;
        return Maze.DIRECTIONS.filter(function (direction) {
            return _this._isInMap(position, direction) && !_this._isConnected(position, direction);
        });
    };
    Maze.prototype._tryHorizontalDirection = function (possibleDirections) {
        return this._getBiasedDirectionIfPossible(possibleDirections, function (direction) {
            return !direction.y;
        });
    };
    Maze.prototype._tryVerticalDirection = function (possibleDirections) {
        return this._getBiasedDirectionIfPossible(possibleDirections, function (direction) {
            return !direction.x;
        });
    };
    Maze.prototype._getBiasedDirectionIfPossible = function (possibleDirections, filterFunction) {
        var pruned = possibleDirections.filter(filterFunction);
        return pruned.length ?
            pruned[this._random(0, pruned.length - 1)] :
            null;
    };
    Maze.prototype._getBiasedDirection = function (possibleDirections) {
        var selected = this._random(1, this._options.horizontalBias + this._options.verticalBias);
        return selected <= this._options.horizontalBias ?
            this._tryHorizontalDirection(possibleDirections) :
            this._tryVerticalDirection(possibleDirections);
    };
    Maze.prototype._getOffsetDirection = function (offset, possibleDirections) {
        var selected = this._random(1, Math.abs(offset.x) + Math.abs(offset.y));
        return selected <= Math.abs(offset.x) ?
            this._tryHorizontalDirection(possibleDirections) :
            this._tryVerticalDirection(possibleDirections);
    };
    Maze.prototype._getStraightDirection = function (position, possibleDirections) {
        var _this = this;
        var potentialDirections = possibleDirections.filter(function (dir) {
            return _this._map.getCellType(position.translate(-dir.x / 2, -dir.y / 2)) === angstloch_1.Cell.TYPE_FLOOR;
        });
        if (potentialDirections.length) {
            return potentialDirections[this._random(0, potentialDirections.length - 1)];
        }
        return null;
    };
    Maze.prototype._getRandomDirection = function (possibleDirections) {
        return possibleDirections[this._random(0, possibleDirections.length - 1)];
    };
    Maze.prototype._isConnected = function (position, direction) {
        return this._map.getCellType(position.translate(direction.x / 2, direction.y / 2)) === angstloch_1.Cell.TYPE_FLOOR;
    };
    return Maze;
}());
Maze.SYMMETRY_NONE = 0;
Maze.SYMMETRY_WHIRLPOOL = 1;
Maze.SYMMETRY_SNOWFLAKE = 2;
Maze.DIRECTIONS = angstloch_1.Offsets.map(function (offset) { return offset.translate(offset); });
exports.Maze = Maze;
